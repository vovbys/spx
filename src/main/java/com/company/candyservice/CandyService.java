package com.company.candyservice;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadPoolExecutor;

import static java.util.Comparator.comparing;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * Сервис пожирания конфет
 */
public class CandyService extends CandyServiceBase {
    private Map<ICandy.Flavour, Queue<ICandyHolder>> queue = new EnumMap<>(ICandy.Flavour.class);
    private Map<ICandyEater, Optional<ICandy.Flavour>> busyEaters = new ConcurrentHashMap<>();
    private ThreadPoolExecutor executorService;


    public CandyService(ICandyEater[] candyEaters) {
        super(candyEaters);
        this.executorService = (ThreadPoolExecutor) newFixedThreadPool(candyEaters.length);

        for (ICandyEater candyEater : candyEaters) {
            busyEaters.put(candyEater, empty());
        }

        for (ICandy.Flavour type : ICandy.Flavour.values()) {
            ConcurrentLinkedQueue<ICandyHolder> candies = new ConcurrentLinkedQueue<>();
            queue.put(type, candies);
        }
    }

    public synchronized void addCandy(ICandy candy) {
        if (queue.get(candy.getCandyFlavour()).isEmpty()) {
            run(candy);
        }
        queue.get(candy.getCandyFlavour()).add(new ICandyHolder(candy));
    }

    private void run(ICandy candy) {
        Optional<Map.Entry<ICandyEater, Optional<ICandy.Flavour>>> any = busyEaters
                .entrySet()
                .stream()
                .filter(p -> !p.getValue().isPresent())
                .findAny();
        if (any.isPresent()) {
            run(any.get().getKey(), candy);
        } else {
            queue.get(candy.getCandyFlavour()).add(new ICandyHolder(candy));
        }
    }

    private void run(ICandyEater eater, ICandy candy) {
        busyEaters.put(eater, of(candy.getCandyFlavour()));
        executorService.execute(createRunnable(eater, candy));
    }

    private Runnable createRunnable(ICandyEater eater, ICandy candy) {
        return () -> {
            try {
                eater.eat(candy);
            } catch (Exception e) {
                addCandy(candy);
            }

            busyEaters.put(eater, empty());
            popCandy().ifPresent(p -> run(eater, p));
        };
    }

    private Optional<ICandy> popCandy() {
        try {
            return queue
                    .entrySet()
                    .stream()
                    .filter(p -> !p.getValue().isEmpty() && !busyEaters.values().contains(of(p.getKey())))
                    .min(comparing(p -> p.getValue().element().getTime()))
                    .map(p -> queue.get(p.getKey()).poll())
                    .map(ICandyHolder::getCandy);
        } catch (NoSuchElementException e) {
            return empty();
        }

    }

    private class ICandyHolder {
        private LocalDateTime time;
        private ICandy candy;

        ICandyHolder(ICandy candy) {
            this.candy = candy;
            this.time = LocalDateTime.now();
        }

        LocalDateTime getTime() {
            return time;
        }

        ICandy getCandy() {
            return candy;
        }
    }
}