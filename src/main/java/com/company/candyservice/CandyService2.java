package com.company.candyservice;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.*;

public class CandyService2 extends CandyServiceBase {
    private final Map<ICandy.Flavour, BlockingQueue<ICandy>> candyQueueMap = new EnumMap<>(ICandy.Flavour.class);
    private final BlockingQueue<BlockingQueue<ICandy>> candyQueueQueue = new ArrayBlockingQueue<>(
            ICandy.Flavour.values().length);

    private ExecutorService executorService;

    public CandyService2(ICandyEater[] candyEaters) {
        super(candyEaters);
    }

    public void start() {
        executorService = Executors.newFixedThreadPool(candyEaters.length);
        for (ICandyEater candyEater : candyEaters) {
            executorService.execute(() -> {
                try {
                    eatCandies(candyEater);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                    e.printStackTrace(); // TODO replace with logging framework
                    throw new RuntimeException(e); // TODO replace with custom exception
                }
            });
        }
    }

    public void stop() throws InterruptedException {
        executorService.shutdownNow();
        executorService.awaitTermination(1, TimeUnit.HOURS);
    }

    @Override
    public synchronized void addCandy(ICandy candy) {
        try {
            BlockingQueue<ICandy> candyQueue = candyQueueMap.get(candy.getCandyFlavour());
            if (candyQueue == null) {
                candyQueue = new LinkedBlockingQueue<>();
                candyQueueMap.put(candy.getCandyFlavour(), candyQueue);
                candyQueueQueue.put(candyQueue);
            }
            candyQueue.put(candy);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e); // TODO replace with custom exception
        }
    }

    private void eatCandies(ICandyEater candyEater) throws Exception {
        while (!Thread.currentThread().isInterrupted()) {
            BlockingQueue<ICandy> candyQueue;
            candyQueue = candyQueueQueue.take();
            while (!Thread.currentThread().isInterrupted()) {
                ICandy candy = candyQueue.take();
                candyEater.eat(candy);
                synchronized (this) {
                    if (candyQueue.isEmpty()) {
                        candyQueueMap.remove(candy.getCandyFlavour());
                        break;
                    }
                }
            }
        }
    }
}
