package com.company.candyservice;

/**
 * Сервис пожирания конфет, требует реализации
 */
public abstract class CandyServiceBase {
    protected ICandyEater[] candyEaters;

    /**
     * Сервис получает при инициализации массив доступных пожирателей конфет
     * @param candyEaters
     */
    public CandyServiceBase(ICandyEater[] candyEaters) {
        this.candyEaters = candyEaters;
    }

    /**
     * Добавить конфету на съедение
     * @param candy
     */
    public abstract void addCandy(ICandy candy);
}
