package com.company.candyservice;


import java.util.concurrent.atomic.AtomicInteger;

/**
 * Конфета, имеет вкус, можно съесть
 */
public interface ICandy {

    /**
     * Получить вкус конфеты
     */
    Flavour getCandyFlavour();

    enum Flavour {
        APPLE,
        BANANA,
        ORANGE,
        CARAMEL,
        VANILLA,
        CHOCOLATE,
    }
}
