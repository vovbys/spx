package com.company.candyservice;

import java.util.concurrent.atomic.AtomicInteger;

public class CandyMock implements ICandy {
    private final Flavour candyFlavour;
    static AtomicInteger ATOMIC_INTEGER =  new AtomicInteger(1);

    public int getOrder() {
        return order;
    }

    private int order;

    public CandyMock(Flavour candyFlavour) {
        this.candyFlavour = candyFlavour;
        this.order = ATOMIC_INTEGER.incrementAndGet();
    }

    @Override
    public String toString() {
        return "CandyMock{" +
                "candyFlavour=" + candyFlavour +
                ", order=" + order +
                '}';
    }

    @Override
    public Flavour getCandyFlavour() {
        return candyFlavour;
    }


}
