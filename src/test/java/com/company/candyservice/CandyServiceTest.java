package com.company.candyservice;

import org.junit.Assert;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class CandyServiceTest {
    private static final int SO_MUCH_CANDIES = 1000000;
    private CountDownLatch eatenCount;

    private CandyService createService(int eaterCount) {


        ICandyEater[] eaters = new ICandyEater[eaterCount];
        for (int i = 0; i < eaters.length; i++) {
            eaters[i] = candy -> {
                Thread.sleep(100);
                eatenCount.countDown();
            };
        }

        return new CandyService(eaters);
    }

    @Test(timeout = 200)
    public void testEatOneCandy() throws InterruptedException {
        eatenCount = new CountDownLatch(1);
        CandyService candyService = createService(1);


        candyService.addCandy(new CandyMock(ICandy.Flavour.APPLE));

        eatenCount.await();

    }

    @Test
    public void testEatInParallel() throws InterruptedException {
        eatenCount = new CountDownLatch(2);
        CandyService candyService = createService(2);


        candyService.addCandy(new CandyMock(ICandy.Flavour.APPLE));
        candyService.addCandy(new CandyMock(ICandy.Flavour.BANANA));

        eatenCount.await();

    }

    @Test(timeout = 300)
    public void testEatInParallelWithDups() throws InterruptedException {
        eatenCount = new CountDownLatch(3);
        CandyService candyService = createService(3);

        candyService.addCandy(new CandyMock(ICandy.Flavour.APPLE));
        candyService.addCandy(new CandyMock(ICandy.Flavour.BANANA));
        candyService.addCandy(new CandyMock(ICandy.Flavour.APPLE));

        Assert.assertFalse(eatenCount.await(150, TimeUnit.MILLISECONDS));

        eatenCount.await();

    }



    @Test(timeout = 3000)
    public void testEatMoreCandiesNotLoosingAny() throws InterruptedException {
        eatenCount = new CountDownLatch(SO_MUCH_CANDIES);

        CandyService candyService = new CandyService(new ICandyEater[]{
                candy -> eatenCount.countDown(),
                candy -> eatenCount.countDown(),
                candy -> eatenCount.countDown()
        });

        ArrayList<ICandy> candies = new ArrayList<>();
        for (int i = 1; i <= SO_MUCH_CANDIES; i++) {
            candies.add(new CandyMock(ICandy.Flavour.values()[
                    ThreadLocalRandom.current().nextInt(ICandy.Flavour.values().length)]));
        }

        candies.parallelStream().forEach(candyService::addCandy);
        eatenCount.await();
    }


    @Test(timeout = 100000)
    public void testComparison() throws InterruptedException {
        eatenCount = new CountDownLatch(SO_MUCH_CANDIES);

        CandyService candyService = new CandyService(new ICandyEater[]{
                candy -> eatenCount.countDown(),
                candy -> eatenCount.countDown(),
                candy -> eatenCount.countDown()
        });

        CandyService2 candyService2 = new CandyService2(new ICandyEater[]{
                candy -> eatenCount.countDown(),
                candy -> eatenCount.countDown(),
                candy -> eatenCount.countDown()
        });

        ArrayList<ICandy> candies = new ArrayList<>();
        for (int i = 1; i <= SO_MUCH_CANDIES; i++) {
            candies.add(new CandyMock(ICandy.Flavour.values()[
                    ThreadLocalRandom.current().nextInt(ICandy.Flavour.values().length)]));
        }



        candyService2.start();
        LocalDateTime start2 = LocalDateTime.now();
        System.out.println(start2);
        candies.parallelStream().forEach(candyService2::addCandy);
        eatenCount.await();
        LocalDateTime stop2 = LocalDateTime.now();
        System.out.println(stop2);
        candyService2.stop();



        eatenCount = new CountDownLatch(SO_MUCH_CANDIES);
        LocalDateTime start1 = LocalDateTime.now();
        System.out.println(start1);
        candies.parallelStream().forEach(candyService::addCandy);
        eatenCount.await();
        LocalDateTime stop1 = LocalDateTime.now();
        System.out.println(stop1);


        System.out.println(Duration.between(start2,stop2));
        System.out.println(Duration.between(start1,stop1));
    }
}
